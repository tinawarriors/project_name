<?php

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect'],
], function () {
    Cache::flush();
    Route::auth();
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () {
            return View::make('welcome');
        });
        Route::get('/application', 'ApplicationController@index');
        Route::get('/admin', ['middleware' => 'admin', 'uses' => 'AdminController@index']);
        Route::get('/stats', ['middleware' => 'admin'], function () {
        });
        Route::get('/home', 'HomeController@index');
        Route::resource('news', 'NewsController');
        Route::patch('/news/{id}', [
            'as' => 'news.update',
            'uses' => 'NewsController@update'
        ]);

        Route::get('/rest', 'RestController@index');
        Route::get('/rest/epub', 'RestController@getEPUB');
        Route::get('/rest/pdf', 'RestController@getPDF');
        Route::get('/soap', 'SoapController@index');
        Route::get('/soap/epub', 'SoapController@getEPUB');
        Route::get('/soap/pdf', 'SoapController@getPDF');
		Route::get('feed', 'FeedController@generate');

        Route::get('/tech', function () {
            return View::make('tech');
        });
        Route::get('/contact', function () {
            return View::make('contact');
        });
    });

});

Route::any('/soap/client', 'SoapController@client');
Route::any('/soap/server', 'SoapController@server');
Route::any('/soap/service.wsdl', 'SoapController@autoDiscovery');
Route::any('/soap/nowsdlserver', 'SoapController@noWSDLserver');
Route::any('/soap/nowsdlclient', 'SoapController@noWSDLclient');


