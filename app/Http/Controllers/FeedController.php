<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FeedController extends Controller
{
    public function generate()
    {
        $posts = \DB::table('news')->orderBy('created_at', 'desc')->take(20)->get();
		
		$feed = \App::make('feed');
		$feed->title = 'RSS';
		$feed->description = 'RSS pre stránku projekt_name';
		$feed->link = url('feed');
		$feed->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
		$feed->pubdate = $posts[0]->created_at;
		$feed->lang = 'sk';
		$feed->setShortening(true); // true or false
		$feed->setTextLimit(100); // maximum length of description text
		
		foreach ($posts as $post)
		{
			//$feed->add($post->nameSK, $post->articleSK);
			$feed->add($post->nameSK, "admin", "http://147.175.98.212/project-name/public/news/".$post->id, $post->created_at, $post->articleSK, $post->articleEN);
		}

		return $feed->render('rss'); // or atom
    }
}
