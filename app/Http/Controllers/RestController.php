<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;

use PHPePub\Core\EPub;
use mPDF;


class RestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('rest');
    }

    public function getPDF()
    {
        $this->createPDF("rest", "REST API");
    }

    public function getEPUB()
    {
        $this->createEPUB("rest", "REST API");
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createPDF($view_name, $header_text)
    {
        $view = View::make($view_name);
        $content = $view->renderSections()["content"];
        $stylesheet = file_get_contents('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css');
        $stylesheet .= ".btn{visibility: hidden; opacity: 0; display: none; width: 0; height:0;}";

        date_default_timezone_set('Europe/Bratislava');
        $date = date('d.m.y H:i:s', time());
        $header = "<div style='margin: 20px;width: 100%; text-align: center;'><h1>$header_text</h1><div>vygenerované: $date</div></div>";

        $mpdf = new mPDF('utf-8');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($header . $content);
        $mpdf->Output();
        exit();
    }

    public function createEPUB($view_name, $header_text)
    {

        $content_start =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            . "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n"
            . "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n"
            . "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
            . "<head>"
            . "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
            . "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css\" />\n"
            . "<style> .btn{display: none;} </style>"
            . "<title>API</title>\n"
            . "</head>\n"
            . "<body>\n";
        $bookEnd = "</body>\n</html>\n";

        $view = View::make($view_name);
        $content = $view->renderSections()["content"];


        $book = new EPub();
        $book->setTitle($header_text);

        date_default_timezone_set('Europe/Bratislava');
        $date = date('d.m.y H:i:s', time());
        $header = "<div style='margin: 20px;width: 100%; text-align: center;'><h1>$header_text</h1><div>vygenerované: $date</div></div>";

        $content = $content_start . $header . $content . $bookEnd;
        $book->addChapter("Chapter 1: Guide", "Chapter001.html", $content, true, EPub::EXTERNAL_REF_ADD);

        $book->finalize();
        $book->sendBook($view_name);
        exit();
    }

}
