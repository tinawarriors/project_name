<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelLocalization;

use App\Http\Requests;

use View;
use App\News;
 use Input;
use Redirect;
use Html;
use Validator;
use Session;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();

        return View::make('news.index')->with('news',$news);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return View::make('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $rules = array(
            'nameSK'       => 'required',
            'nameEN'      => 'required',
            'articleSK' => 'required',
            'articleEN' => 'required'
        );
       $validator = Validator::make(Input::all(), $rules);
       if ($validator->fails()) {
            return Redirect::to('news/create')
                ->withErrors($validator);
        } else {
            $news  = new News;
            $news->nameSK       = Input::get('nameSK');
            $news->nameEN       = Input::get('nameEN');
            $news->articleSK       = Input::get('articleSK'); 
            $news->articleEN       = Input::get('articleEN');
            $news->save();
			
            Session::flash('message', 'news.success');
            return Redirect::to(LaravelLocalization::getCurrentLocale().'/news');
         }
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
        return View::make('news.show')
            ->with('news', $news); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return View::make('news.edit')
            ->with('news', $news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'nameSK'       => 'required',
            'nameEN'      => 'required',
            'articleSK' => 'required',
            'articleEN' => 'required'
        );
       $validator = Validator::make(Input::all(), $rules);
       if ($validator->fails()) {
            return Redirect::to('news/'.$id.'/edit')
                ->withErrors($validator);
        } else {    
            $news  = News::find($id);
            $news->nameSK       = Input::get('nameSK');
            $news->nameEN       = Input::get('nameEN');
            $news->articleSK       = Input::get('articleSK');
            $news->articleEN       = Input::get('articleEN');
            $news->save();

            Session::flash('message', 'news.successupdate');
            return Redirect::to(LaravelLocalization::getCurrentLocale().'/news');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $news = News::find($id);
        $news->delete();

        // redirect
        Session::flash('message', 'news.delete');
        return Redirect::to(LaravelLocalization::getCurrentLocale().'/news');
    }
}
