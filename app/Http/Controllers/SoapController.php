<?php

//global namespace, kvoli registerPartialHandler aby Octave_partial_processor mal pristup k funkcii ...
namespace {

    $data = array();
    $n = 0;
    $min = 0;
    $max = 0;
    $krok = 0;
    $i = 0;
    $function = "x";

    function process_partial($payload, $partial)
    {
        global $data, $i, $krok, $min;
        $parts = explode(" ", $payload);
        foreach ($parts as $y) {
            if (empty($y)) {
                continue;
            }
            $i++;

            if (round($min, 2) == 0)
                $min += $krok;

            $data["" . floatval($min)] = floatval($y);

            $min += $krok;
            ob_flush();
            flush();
        }
    }

    function octaveCalculateDerivation($local_function, $local_n, $local_min, $local_max)
    {
        global $data;
        $data = array();
        $query = "diff(x.*" . $local_function . ")./ diff(x)";
        octaveCalculateFunction($query, $local_n, $local_min, $local_max);
        return $data;
    }

    function octaveCalculateFunction($local_function, $local_n, $local_min, $local_max)
    {
        global $data, $krok, $i, $function, $n, $min, $max;
        $data = array();

        $function = $local_function;
        $n = $local_n;
        $min = $local_min;
        $max = $local_max;
        $krok = ($max - $min) / ($n - 1);

        $o = new Octave(false);
        $o->init();
        $o->registerPartialHandler("process_partial");
        $o->run("x = linspace($min, $max, $n);");
        $o->query($function);

        return $data;
    }

}

namespace App\Http\Controllers {

    ini_set('soap.wsdl_cache_enabled', 0);


    use Illuminate\Http\Request;

    use App\Http\Requests;
    use View;

    use Zend\Soap\AutoDiscover;
    use Zend\Soap\Server;
    use Zend\Soap\Client;

    use Octave;


//    i know ...
// musi byt mimo triedy MySoap aby sa neobjavila vo vygenerovanom wsdl
    function checkKey($key)
    {
        if ($key !== "warriors")
            throw new \SoapFault("Ivalid auth key: " . $key, $key);
    }


    class MySoap
    {
        /**
         * This function ...
         *
         * @param string $key
         * @param string $function
         * @param integer $n
         * @param integer $min
         * @param integer $max
         * @return array
         */
        function calculateFunction($key, $function, $n, $min, $max)
        {
            checkKey($key);
            return octaveCalculateFunction($function, $n, $min, $max);
        }

        /**
         * This function ...
         *
         * @param string $key
         * @param string $function
         * @param integer $n
         * @param integer $min
         * @param integer $max
         * @return array
         */
        function calculateDerivation($key, $function, $n, $min, $max)
        {
            checkKey($key);
            return octaveCalculateDerivation($function, $n, $min, $max);
        }

    }

    class SoapController extends Controller
    {


        public function index()
        {
            return View::make('soap');
        }

        public function getPDF()
        {
            app('App\Http\Controllers\RestController')->createPDF("soap", "SOAP API");
        }

        public function getEPUB()
        {
            app('App\Http\Controllers\RestController')->createEPUB("soap", "SOAP API");
        }


        public function autoDiscovery()
        {
            $autodiscover = new AutoDiscover();

            $autodiscover
                ->setClass(MySoap::class)
                ->setUri(url('/soap/server'))
                ->setServiceName('MySoapService');

            header('Content-Type: application/wsdl+xml');
            echo $autodiscover->toXml();
            die();

        }

        public function server()
        {
            $server = new Server();
            $server->setWSDL(url("soap/service.wsdl"));
            $server->setClass(MySoap::class);
            $server->handle();

        }

        public function client()
        {

//            print_r(octaveCalculateFunction("x", 10, 10, 100));
//            print_r(octaveCalculateDerivation("x", 10, 10, 100));

            $client = new Client(url("soap/service.wsdl"));
            $result = $client->calculateFunction("warriors", "x", 10, 0, 10);
            var_dump($result);
        }


        public function noWSDLclient()
        {

            $options = array(
                "uri" => url('soap/server'),
                "location" => url('soap/server'),
                "soap_version" => SOAP_1_2
            );

            $client = new Client(null, $options);
            $result = $client->calculateFunction("warriors", "x", 10, 0, 10);
            var_dump($result);
        }


    }

}