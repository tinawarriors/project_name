@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('soap.header') }}</div>
                    <div class="panel-body">
                        <p>
                            {{ trans('soap.uses_library') }}<a href="https://zendframework.github.io/zend-soap/">Zend\Soap</a>
                        </p>
                        <p>
                            server url: <a
                                    href="<?php echo url('/soap/server');?>"><?php echo url('/soap/server');?></a>
                        </p>
                        <p>
                            wsdl url: <a
                                    href="<?php echo url('/soap/service.wsdl');?>"><?php echo url('/soap/service.wsdl');?></a>
                        </p>
                        <p>
                            api key: <strong>warriors</strong>
                        </p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('soap.functions') }}</div>
                    <div class="panel-body">

                        <p>
                            <strong>calculateFunction($key, $function, $n, $min, $max)</strong>
                        <ul>
                            <li><span class="param-type">string:</span> <strong>$key</strong>
                                - {{ trans('soap.parameter_key') }}</li>
                            <li><span class="param-type">string:</span> <strong>$function</strong>
                                - {{ trans('soap.parameter_function') }} </li>
                            <li><span class="param-type">int:</span> <strong>$n</strong> - {{ trans('soap.parameter_n') }}
                            </li>
                            <li><span class="param-type">float:</span> <strong>$min</strong>
                                - {{ trans('soap.parameter_min') }}</li>
                            <li><span class="param-type">float:</span> <strong>$max</strong>
                                - {{ trans('soap.parameter_max') }}</li>
                        </ul>
                        <p class="text-info">{{ trans('soap.return_value_function') }}</p>
                        </p>

                        <p>
                            <strong>calculateDerivation($key, $function, $n, $min, $max)</strong>
                        <ul>
                            <li><span class="param-type">string:</span> <strong>$key</strong>
                                - {{ trans('soap.parameter_key') }}</li>
                            <li><span class="param-type">string:</span> <strong>$function</strong>
                                - {{ trans('soap.parameter_function') }} </li>
                            <li><span class="param-type">int:</span> <strong>$n</strong> - {{ trans('soap.parameter_n') }}
                            </li>
                            <li><span class="param-type">float:</span> <strong>$min</strong>
                                - {{ trans('soap.parameter_min') }}</li>
                            <li><span class="param-type">float:</span> <strong>$max</strong>
                                - {{ trans('soap.parameter_max') }}</li>
                        </ul>
                        <p class="text-info">{{ trans('soap.return_value_derivation') }}</p>
                        </p>


                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('soap.examples') }}</div>
                    <div class="panel-body">

                        <div>
                            <h4>{{ trans('soap.withwsdl') }}</h4>
                            <code>
                                use Zend\Soap\Client;
                                <br>...<br>
                                //initialize soap client<br>
                                $client = new Client(<?php echo url("soap/service.wsdl") ?>);<br>
                                //call service function<br>
                                $result = $client->calculateFunction("warriors", "x", 10, 0, 10);<br>
                            </code>
                        </div>

                        <h4>{{ trans('soap.withoutwsdl') }}</h4>
                        <code>
                            use Zend\Soap\Client;
                            <br>...<br>
                            $options = array(<br>
                            "uri" => <?php echo url("soap/server") ?>,<br>
                            "location" => <?php echo url("soap/server") ?>,<br>
                            "soap_version" => SOAP_1_2);<br><br>

                            //initialize soap client<br>
                            $client = new Client(null, $options);<br>
                            //call service function<br>
                            $result = $client->calculateDerivation("warriors", "x", 10, 0, 10);<br>
                        </code>
                    </div>

                    <a href="soap/pdf" target="_blank" class="btn btn-success btn-block" role="button">
                        {{ trans('soap.pdf') }}
                    </a>
                    <a href="soap/epub" target="_blank" class="btn btn-success btn-block" role="button">
                        {{ trans('soap.epub') }}
                    </a>
                </div>
            </div>
        </div>
@endsection
