@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('rest.header') }}</div>

                    <div class="panel-body">
                        <p>
                            http://147.175.98.212/project-name/api/rest.php/&lt;RESOURCE&gt;/&lt;FUNTION&gt;?key=&lt;KEY&gt;&amp;min=&lt;MIN&gt;&amp;max=&lt;MAX&gt;&amp;n=&lt;N&gt;
                        </p>
                        <table class="table table-hover" id="task-table">
                            <thead>
                            <tr>
                                <th>{{ trans('rest.parameter') }}</th>
                                <th>{{ trans('rest.description') }}</th>
                                <th>{{ trans('rest.value') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>RESOURCE</td>
                                <td>{{ trans('rest.resource') }}</td>
                                <td>function/derivation</td>
                            <tr>
                            <tr>
                                <td>FUNCTION</td>
                                <td>{{ trans('rest.function') }}</td>
                                <td>f(x)</td>
                            <tr>
                            <tr>
                                <td>KEY</td>
                                <td>{{ trans('rest.key') }}</td>
                                <td>warriors</td>
                            <tr>
                            <tr>
                                <td>MIN</td>
                                <td>{{ trans('rest.min') }}</td>
                                <td>float</td>
                            <tr>
                            <tr>
                                <td>MAX</td>
                                <td>{{ trans('rest.max') }}</td>
                                <td>float</td>
                            <tr>
                            <tr>
                                <td>N</td>
                                <td>{{ trans('rest.n') }}</td>
                                <td>int, N &gt= 2</td>
                            <tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('rest.request') }}</div>

                    <div class="panel-body">
                        <p>
                            http://147.175.98.212/bordel/project_name/api/rest.php/function/x?key=warriors&min=1&max=3&n=3
                        </p>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('rest.response') }}</div>

                    <div class="panel-body">
                        <p>
                            data:{"status":"partial","x":1,"y":1} <br>
                            data:{"status":"partial","x":2,"y":2} <br>
                            data:{"status":"partial","x":3,"y":3} <br>
                            data:{"status":"success"}
                        </p>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('rest.errors') }}</div>

                    <div class="panel-body">
                        <table class="table table-hover" id="task-table">
                            <thead>
                            <tr>
                                <th>{{ trans('rest.error') }}</th>
                                <th>{{ trans('rest.example') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>RESOURCE</td>
                                <td>data:{"status":"error","description":"Resource error."}</td>
                            <tr>
                            <tr>
                                <td>FUNCTION</td>
                                <td>data:{"status":"error","description":"Function error."}</td>
                            <tr>
                            <tr>
                                <td>MIN, MAX, N</td>
                                <td>data:{"status":"error","description":"Parameters error."}</td>
                            <tr>
                            <tr>
                                <td>KEY</td>
                                <td>data:{"status":"error","description":"API key error."}</td>
                            <tr>

                            </tbody>
                        </table>


                    </div>
                </div>


                <a href="rest/pdf" target="_blank" class="btn btn-success btn-block" role="button">
                    {{ trans('rest.pdf') }}
                </a>
                <a href="rest/epub" target="_blank" class="btn btn-success btn-block" role="button">
                    {{ trans('rest.epub') }}
                </a>
            </div>
        </div>
    </div>
@endsection
