@extends('layouts.app')
@section('head')
<style>
                .row{
		    margin-top:40px;
		    padding: 0 10px;
		}
		.clickable{
		    cursor: pointer;   
		}

		.panel-heading div {
			margin-top: -18px;
			font-size: 15px;
		}
		.panel-heading div span{
			margin-left:5px;
		}
		.panel-body{
			display: none;
		}
</style>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('contact.header') }}</div>
            </div>
            <div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">{{ trans('contact.task')  }}</h3>
						<div class="pull-right">
							<span class="clickable filter" data-toggle="tooltip" title="{{ trans('contact.togglefilter') }}" data-container="body">
								<i class="glyphicon glyphicon-filter"></i>
							</span>
						</div>
					</div>
					<div class="panel-body">
						<input type="text" class="form-control" id="task-table-filter" data-action="filter" data-filters="#task-table" placeholder="{{ trans('contact.filterplaceholder') }}" />
					</div>
					<table class="table table-hover" id="task-table">
						<thead>
							<tr>
								<th>#</th>
								<th>{{ trans('contact.taskcolumns') }}</th>
								<th>{{ trans('contact.assignee') }}</th>
								<th>{{ trans('contact.status') }}</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>{{ trans('task.instalation') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
							<tr>
								<td>2</td>
								<td>{{ trans('task.gitinstalation') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
                                                        <tr>
                                                                <td>3</td>
                                                                <td>{{ trans('task.project') }}</td>
                                                                <td>Kamil Pavličko</td>
                                                                <td>{{ trans('task.done') }}</td>
                                                        </tr>
							<tr>
								<td>4</td>
								<td>{{ trans('task.localization') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
							<tr>
								<td>5</td>
								<td>{{ trans('task.menu') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
							<tr>
								<td>6</td>
								<td>{{ trans('task.layout') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
							<tr>
								<td>7</td>
								<td>{{ trans('task.tracker') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
							<tr>
								<td>8</td>
								<td>{{ trans('task.authentification') }}</td>
								<td>Kamil Pavličko</td>
								<td>{{ trans('task.done') }}</td>
							</tr>
                            <tr>
                                <td>9</td>
                                <td>{{ trans('task.news') }}</td>
                                <td>Kamil Pavličko</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>{{ trans('task.rest') }}</td>
                               	<td>Kristián Duda</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>{{ trans('task.octave') }}</td>
                               	<td>Kristián Duda</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>{{ trans('task.charts') }}</td>
                               	<td>Kristián Duda</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>{{ trans('task.csv') }}</td>
                               	<td>Kristián Duda</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>{{ trans('task.rss') }}</td>
                                <td>Miroslav Hegedüš</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>{{ trans('task.newsletter') }}</td>
                                <td>Miroslav Hegedüš</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
							<tr>
                                <td>16</td>
                                <td>{{ trans('task.pdf') }}</td>
                                <td>Michal Češek</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
							<tr>
                                <td>17</td>
                                <td>{{ trans('task.epub') }}</td>
                                <td>Michal Češek</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
			    <tr>
                                <td>18</td>
                                <td>{{ trans('task.soap') }}</td>
                                <td>Michal Češek</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td>{{ trans('task.tech') }}</td>
                                <td>Kamil Pavličko, Kristián Duda</td>
                                <td>{{ trans('task.done') }}</td>
                            </tr>
						</tbody>
					</table>
				</div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
(function(){
    'use strict';
	var $ = jQuery;
	$.fn.extend({
		filterTable: function(){
			return this.each(function(){
				$(this).on('keyup', function(e){
					$('.filterTable_no_results').remove();
					var $this = $(this), 
                        search = $this.val().toLowerCase(), 
                        target = $this.attr('data-filters'), 
                        $target = $(target), 
                        $rows = $target.find('tbody tr');
                        
					if(search == '') {
						$rows.show(); 
					} else {
						$rows.each(function(){
							var $this = $(this);
							$this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
						})
						if($target.find('tbody tr:visible').size() === 0) {
							var col_count = $target.find('tr').first().find('td').size();
							var no_results = $('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No results found</td></tr>')
							$target.find('tbody').append(no_results);
						}
					}
				});
			});
		}
	});
	$('[data-action="filter"]').filterTable();
})(jQuery);

$(function(){
    // attach table filter plugin to inputs
	$('[data-action="filter"]').filterTable();
	
	$('.container').on('click', '.panel-heading span.filter', function(e){
		var $this = $(this), 
			$panel = $this.parents('.panel');
		
		$panel.find('.panel-body').slideToggle();
		if($this.css('display') != 'none') {
			$panel.find('.panel-body input').focus();
		}
	});
	$('[data-toggle="tooltip"]').tooltip();
})
</script>
@endsection
