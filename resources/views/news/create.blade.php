@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('news.header') }}</div>

                <div class="panel-body">
                {{ trans('news.new') }}
                </div>
            </div>
            <h1>{{ trans('news.newarticle') }}</h1>

                {{ Html::ul($errors->all()) }}
		{{ Form::open(array('url' => 'news')) }}

		    <div class="form-group">
			{{ Form::label('nameSK', 'NameSK') }}
			{{ Form::text('nameSK', Input::old('nameSK'), array('class' => 'form-control')) }}
		    </div>

		    <div class="form-group">
			{{ Form::label('nameEN', 'NameEN') }}
			{{ Form::text('nameEN', Input::old('nameEN'), array('class' => 'form-control')) }}
		    </div>

		    <div class="form-group">
			{{ Form::label('articleSK', 'ArticleSK') }}
			{{ Form::text('articleSK', Input::old('articleSK'), array('class' => 'form-control')) }}
		    </div>

		    <div class="form-group">
			{{ Form::label('articleEN', 'ArticleEN') }}
			{{ Form::text('articleEN', Input::old('articleEN'), array('class' => 'form-control')) }}
		    </div>

		    {{ Form::submit(trans('news.publish'), array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
        </div>
    </div>
</div>
@endsection
