@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1>{{ trans('news.newarticle') }}</h1>

                {{ Html::ul($errors->all()) }}
                    {{ Form::model($news, array('route' => array('news.update',$news->id), 'method' => 'PUT' )) }}
                    <div class="form-group">
                        {{ Form::label('nameSK', 'NameSK') }}
                        {{ Form::text('nameSK', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('nameEN', 'NameEN') }}
                        {{ Form::text('nameEN', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('articleSK', 'ArticleSK') }}
                        {{ Form::text('articleSK', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('articleEN', 'ArticleEN') }}
                        {{ Form::text('articleEN', null, array('class' => 'form-control')) }}
                    </div>

                    {{ Form::submit(trans('news.publish'), array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
