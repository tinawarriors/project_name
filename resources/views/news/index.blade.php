@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('news.header') }}</div>

                <div class="panel-body">
                {{ trans('news.title') }}
                </div>
            </div>
           @if (Auth::user()->isAdmin())
                  <a href="{{url(LaravelLocalization::getCurrentLocale().'/news/create')}}" class="btn btn-primary  btn-block" role="button">
                     {{ trans('news.new') }}
                  </a>
           @endif
           <a href="http://147.175.98.212/project-name/Mail/index.php" class="btn btn-primary  btn-block" role="button">
                     {{ trans('news.newsletter') }}
                  </a>
           @if (Session::has('message'))
               <br>
               <div class="alert alert-info">{{ trans(Session::get('message')) }}</div>
           @endif
           @foreach ($news as $item)
               <div style="margin:20px;" class="panel panel-default">
               <div class="panel-heading">
               @if (LaravelLocalization::getCurrentLocale() == "sk")
                   {{ $item ->nameSK }}
               @else
                   {{ $item ->nameEN }}
               @endif
               </div>

               <div class="panel-body">
               @if (LaravelLocalization::getCurrentLocale() == "sk")
                   {{ $item ->articleSK }}
               @else
                   {{ $item ->articleEN }}
               @endif
	       <br><br>
           @if (Auth::user()->isAdmin())
                {{ Form::open(array('url' => 'news/' . $item->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit(trans('news.deleteitem'), array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
           @endif
               <a href="{{url(LaravelLocalization::getCurrentLocale().'/news/'.$item->id)}}" class="btn btn-primary" role="button">
                  {{ trans('news.show') }}
               </a>
             @if (Auth::user()->isAdmin())
               <a href="{{url(LaravelLocalization::getCurrentLocale().'/news/'.$item->id.'/edit')}}" class="btn btn-info" role="button">
                  {{ trans('news.edit') }}
               </a>
             @endif
               </div>
               </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
