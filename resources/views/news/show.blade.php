@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <h1 style="text-align:center;">
            @if (LaravelLocalization::getCurrentLocale() == "sk")
                   {{ $news ->nameSK }}
            @else
                   {{ $news ->nameEN }}
            @endif
            </h1>

            <div class="jumbotron text-center">
               <p>
                   @if (LaravelLocalization::getCurrentLocale() == "sk")
                      {{ $news ->articleSK }}
                   @else
                      {{ $news ->articleEN }}
                   @endif
                 <br>
               </p>
            </div>
            <a href="{{url(LaravelLocalization::getCurrentLocale().'/news')}}" class="btn btn-primary  btn-block" role="button">
                  {{ trans('news.back') }}
               </a>
        </div>
    </div>
</div>
@endsection
