@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('application.header') }}</div>

                <div class="panel-body">
 			<form id ="params" role="form">
				<div class="form-group">
			    		<label for="function">{{ trans('application.function') }}</label>
			      		<input type="text" class="form-control" id="function" placeholder="f(x)" required>
			    	</div>
	    			<div class="form-group">
		    			<label class="radio-inline">
		      			<input type="radio" name="opt" value="function" checked>{{ trans('application.function') }}
		    			</label>
		    			<label class="radio-inline">
		      			<input type="radio" value="derivation" name="opt">{{ trans('application.derivation') }}
		    			</label>
		 		</div>
				<div class="form-group">
			      		<label for="min">{{ trans('application.min') }}</label>
			        	<input type="number" class="form-control" id="min" required>
			    	</div>
			    	<div class="form-group">
			      		<label for="max">{{ trans('application.max') }}</label>
			        	<input type="number" class="form-control" id="max" required>
			    	</div>
			    	<div class="form-group">
			      		<label for="n">{{ trans('application.n') }}</label>
			        	<input type="number" class="form-control" id="n" value="200" min="2" required>
			    	</div>
	    			<button type="submit" class="btn btn-success btn-block">{{ trans('application.submit') }}</button>
	  			<button id="export" type="button" class="btn btn-success btn-block">{{ trans('application.export') }}</button>
			</form> 
	  		<div id="result"></div>       
		</div>
            </div>
            <div id="chart_div"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script>
			google.charts.load('current', {packages: ['corechart', 'line']});
			google.charts.setOnLoadCallback(draw);

			function makeUrl(base, opt, funkcia, params) {
		      	var first = true;
		      	var url = base +"/" +opt +"/" +funkcia +"?";
				Object.keys(params).forEach(function (key) {
					if (first)
						first = false
					else
						url = url + "&";
					url = url +key +"=" +params[key];

				});
				return url;
			}

		    var working= false;
		    var data=null;

			function dataTableToCSV(dataTable_arg) {
			    var dt_cols = dataTable_arg.getNumberOfColumns();
			    var dt_rows = dataTable_arg.getNumberOfRows();
			    
			    var csv_cols = [];
			    var csv_out;
			    
			    // Iterate columns
			    for (var i=0; i<dt_cols; i++) {
			        // Replace any commas in column labels
			        csv_cols.push(dataTable_arg.getColumnLabel(i).replace(/,/g,""));
			    }
			    
			    // Create column row of CSV
			    csv_out = csv_cols.join(";")+"\r\n";
			    
			    // Iterate rows
			    for (i=0; i<dt_rows; i++) {
			        var raw_col = [];
			        for (var j=0; j<dt_cols; j++) {
			            // Replace any commas in row values
			            raw_col.push(dataTable_arg.getFormattedValue(i, j, 'label').replace(/,/g,""));
			        }
			        // Add row to CSV text
			        csv_out += raw_col.join(";")+"\r\n";
			    }

			    return csv_out;
			}

			function downloadCSV (csv_out) {
	            var blob = new Blob([csv_out], {type: 'text/csv;charset=utf-8'});
	            var url  = window.URL || window.webkitURL;
	            var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
	            link.href = url.createObjectURL(blob);
	            link.download = "data.csv"; 

	            var event = document.createEvent("MouseEvents");
	            event.initEvent("click", true, false);
	            link.dispatchEvent(event); 
			}

			function draw() {
				$("#params").submit(function() {
					if(working) {
						return false;
					}
					working=true;
					var funkcia = $('#function').val();
					var opt = $('input[name=opt]:checked').val();
					var params=[];
					//params.funkcia=$('#function').val();
					//params.opt = $('input[name=opt]:checked').val();
					params.min = $('#min').val();
					params.max = $('#max').val();
					params.n = $('#n').val();
					params.key = '{!! env("API_KEY") !!}';

					var options = {
				        hAxis: {
				        	title: 'x',
				    		viewWindow: {
				        		min: params.min,
				        		max: params.max
				    		}
						},
				        vAxis: {
				          title: funkcia
				        }
				    };

					data = new google.visualization.DataTable();
				    data.addColumn('number', 'x');
				    data.addColumn('number', funkcia);

					if(typeof(EventSource) !== "undefined") {
						var url = makeUrl("http://147.175.98.212/project-name/api/rest.php", opt, funkcia, params);
						//rest.php/function/x?min=1&max=58&n=50
					    var source = new EventSource(url);
					    $('#result').text('Working.');
					    source.onmessage = function(event) {
					 		var res = JSON.parse(event.data); 
					 		switch(res.status) {
					 			case "partial":
					 				data.addRow([res.x, res.y]);
						        	var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		      						chart.draw(data, options);
					 				break;
					 			case "success":
					 				event.target.close();
					        		working=false;
					        		$('#result').text('Done.');
					        		break;
					        	case "error":
					        		event.target.close();
					        		working=false;
					        		$('#result').text(res.description);
					        		break;
					        	default:
					        		event.target.close();
					        		working=false;
					        		$('#result').text('Error.');
					        		break;
					 		}
					    };
					} else {
					    $('#result').text('Not supported.');
					}
					return false;
				});
	    	}

	    	$("#export").click(function() {
				if (data==null || working) {
					$('#result').text('Generate firstly.');
					return;
				}
				var csv = dataTableToCSV(data);
				downloadCSV(csv);
			});
		</script>
@endsection
