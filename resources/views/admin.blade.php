@extends('layouts.app')
@section('head')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script type='text/javascript' src="{{ asset('gmaps.js') }}"></script>
@endsection


@section('content')

<div style="margin-bottom:50px" class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('admin.header') }}</div>
            </div>
            <a href="{{url('/stats')}}" class="btn btn-primary  btn-block" role="button">
                {{ trans('admin.detail') }}
            </a>
            <a href="http://147.175.98.212/project-name/Mail/send_news.php" class="btn btn-primary  btn-block" role="button">{{ trans('admin.newsletter') }}</a>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>IP</th>
                    <th>{{ trans('admin.republic') }}</th>
                    <th>{{ trans('admin.action') }}</th>
                </thead>
                <tbody>
                @foreach ($session as $item)
                <tr>   
                     <td>{{ $item->client_ip }}</td>
                     <td>{{ $item->country_name }}</td>
                     <td>{{ $item->count }}</td>
		</tr>
                @endforeach 
                </table>
                <?php /* === THIS IS WHERE WE WILL ADD OUR MAP USING JS ==== */ ?>
                        <div class="google-map-wrap" itemscope itemprop="hasMap" itemtype="http://schema.org/Map">
                            <div id="google-map" class="google-map">
                            </div><!-- #google-map -->
                        </div>

                        <?php /* === MAP DATA === */ ?>
                        <?php


                        $locations = array();


                        foreach ($session as $item){
                            $locations[] = array(
                                'google_map' => array(
                                    'lat' => $item->latitude,
                                    'lng' => $item->longitude,
                                )
                            );
                         }
                         /* Marker #1 */

                        ?>


                        <?php /* === PRINT THE JAVASCRIPT === */ ?>

                        <?php
                        /* Set Default Map Area Using First Location */
                        $map_area_lat = isset( $locations[0]['google_map']['lat'] ) ? $locations[0]['google_map']['lat'] : '';
                        $map_area_lng = isset( $locations[0]['google_map']['lng'] ) ? $locations[0]['google_map']['lng'] : '';
                        ?>

                        <script>
                            jQuery( document ).ready( function($) {

                                /* Do not drag on mobile. */
                                var is_touch_device = 'ontouchstart' in document.documentElement;

                                var map = new GMaps({
                                    el: '#google-map',
                                    lat: '<?php echo $map_area_lat; ?>',
                                    lng: '<?php echo $map_area_lng; ?>',
                                    scrollwheel: false,
                                    draggable: ! is_touch_device
                                });

                                /* Map Bound */
                                var bounds = [];

                                <?php /* For Each Location Create a Marker. */
                                foreach( $locations as $location ){
                                $map_lat = $location['google_map']['lat'];
                                $map_lng = $location['google_map']['lng'];
                                ?>
                                /* Set Bound Marker */
                                var latlng = new google.maps.LatLng(<?php echo $map_lat; ?>, <?php echo $map_lng; ?>);
                                bounds.push(latlng);
                                /* Add Marker */
                                map.addMarker({
                                    lat: <?php echo $map_lat; ?>,
                                    lng: <?php echo $map_lng; ?>
                                });
                                <?php } //end foreach locations ?>

                                /* Fit All Marker to map */
                                map.fitLatLngBounds(bounds);

                                /* Make Map Responsive */
                                var $window = $(window);
                                function mapWidth() {
                                    var size = $('.google-map-wrap').width();
                                    $('.google-map').css({width: size + 'px', height: (size/2) + 'px'});
                                }
                                mapWidth();
                                $(window).resize(mapWidth);
                       });
           </script>
        </div>
    </div>
</div>
@endsection

