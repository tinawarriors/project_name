@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Octave</div>

                <div class="panel-body">
        	        {{ trans('tech.octave') }} <br>
			apt-get install octave <br>
                        https://github.com/Gutza/octave-daemon
                {{ trans('tech.title') }}
                <br> 
                URL: http://147.175.98.212/project-name/public/login
                <br>
                SERVER: 147.175.98.212 
                <br>
                BITBUCKET GIT: https://bitbucket.org/tinawarriors/project_name
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Google Charts</div>

                <div class="panel-body">
                        {{ trans('tech.charts') }} <br>
            		https://developers.google.com/chart/
		</div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">PHPePub</div>

                <div class="panel-body">
                        {{ trans('tech.ePub') }} <br>
                        https://github.com/Grandt/PHPePub
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">mPDF</div>

                <div class="panel-body">
                        {{ trans('tech.pdf') }} <br>
                	https://mpdf.github.io/
		</div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">zend-soap</div>

                <div class="panel-body">
                        {{ trans('tech.soap') }} <br>
                        https://zendframework.github.io/zend-soap/
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Mailer</div>

                <div class="panel-body">
                        {{ trans('tech.mail') }} <br>
			apt-get install sendmail <br> 
			apt-get install mailutils
		</div>
             </div>
             <div class="panel panel-default">  
                <div class="panel-heading">Log</div>
                <div class="panel-body">
                abb66ac - kamil.pavlicko@gmail.com, 20 minutes ago : Pushing Master repo with final changes including tech report
<br>a9eea13 - kamil.pavlicko@gmail.com, 27 minutes ago : Add newsletter, verification and localization for authorization
<br>b01250a - kamil.pavlicko@gmail.com, 41 minutes ago : Merge branch 'master' of bitbucket.org:tinawarriors/project_name
<br>f1262fe - kamil.pavlicko@gmail.com, 43 minutes ago : pridana technicka dokumentacia
<br>a0979c4 - cesek, 85 minutes ago : Merge branch 'master' of bitbucket.org:tinawarriors/project_name
<br>c76335b - cesek, 3 hours ago : complete soap page translation, fix pdf and epub generation date
<br>acfd054 - kamil.pavlicko@gmail.com, 28 hours ago : API_KEY presunuty do konfiguraku
<br>21014a6 - kamil.pavlicko@gmail.com, 7 days ago : RSS generating
<br>d3a27ae - kamil.pavlicko@gmail.com, 7 days ago : doplnene RSS
<br>f597558 - cesek, 11 days ago : add soap service, pdf and epub generator
<br>cf69998 - kamil.pavlicko@gmail.com, 3 weeks ago : rest osetrenie parametra n
<br>e0f4cf3 - kamil.pavlicko@gmail.com, 3 weeks ago : stranka Rest
<br>fac808a - kamil.pavlicko@gmail.com, 3 weeks ago : uprava vzhladu stranky aplikacie
<br>8007234 - kamil.pavlicko@gmail.com, 3 weeks ago : uprava rest (v pripade uspechu status success namiesto end), upratanie stranky aplikacie
<br>41a9923 - kamil.pavlicko@gmail.com, 3 weeks ago : generovanie grafov, export do CSV, stranka aplikacie
<br>b5af71a - kamil.pavlicko@gmail.com, 3 weeks ago : Deleted mess files in project (key, key.pub)
<br>ve5a28d7 - kamil.pavlicko@gmail.com, 3 weeks ago : octave, rest
<br>ad7ae4f - kamil.pavlicko@gmail.com, 3 weeks ago : Pridane tasky
<br>vbe0f2c2 - kamil.pavlicko@gmail.com, 3 weeks ago : Sharing test on multiple repositories
<br>b9c40cc - kamil.pavlicko@gmail.com, 3 weeks ago : Testing git remote repository
<br>b3d0665 - kamil.pavlicko@gmail.com, 3 weeks ago : initial commit
<br>ae24ede - kamil.pavlicko@gmail.com, 3 weeks ago : vendor deleted
<br>00dbbd4 - kamil.pavlicko@gmail.com, 3 weeks ago : added vendor folder
<br>6603733 - kamil.pavlicko@gmail.com, 3 weeks ago : Fix non-admin ability to delete article
<br>09e42f0 - kamil.pavlicko@gmail.com, 3 weeks ago : title change
<br>98aa373 - kamil.pavlicko@gmail.com, 3 weeks ago : Migrating project to new repository

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
