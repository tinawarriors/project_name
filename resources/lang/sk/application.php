<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Aplikácia',
    'function' => 'Funkcia',
    'derivation' => 'Derivácia',
    'min' => 'Minimum',
    'max' => 'Maximum',
    'n' => 'Počet bodov',
    'submit' => 'Vykresliť',
    'export' => 'Export',
    'working' => 'Pracujem',
    'done' => 'Vygenerované',
    'error' => 'Chyba',
    'notsupported' => 'Nepodporované',
    'generateFirstly' => 'Najprv vygeneruj graf',


];
