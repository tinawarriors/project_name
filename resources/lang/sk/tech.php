<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Technická správa',
    'title' => 'Tu budú informácie o technických záležitostiach',
    'octave' => 'Výpočet funkcií',
    'charts' => 'Vykresľovanie grafov',
    'ePub' => 'Generovanie ePub',
    'pdf' => 'Generovanie PDF',
    'soap' => 'Soap',
    'mail' => 'Newsletter',
    'title' => 'Projekt je založený na frameworku Laravel 5',
];
