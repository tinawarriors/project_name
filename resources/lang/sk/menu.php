<?php

return [
    'application' => 'Aplikácia',
    'rest'        => 'Rest',
    'soap'        => 'Soap',
    'news'        => 'Aktuality',
    'tech'        => 'Tech. report',
    'contact'     => 'Kontakt',
    'home'        => 'Domov',
    'login'       => 'Prihlásiť sa',
    'register'    => 'Zaregistrovať sa',
	'rss'         => 'RSS'
];     
