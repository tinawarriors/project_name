<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'done' => 'Spravené',
    'inprocess' => 'Pracuje sa tom',
    'instalation' => 'Inštalácia prostredia',
    'gitinstalation' => 'Inštalácia GIT',
    'project' => 'Založenie a príprava celého projektu',
    'localization' => 'Lokalizácia',
    'menu' => 'Dynamické menu',
    'layout' => 'Vytvorenie layout-u pre aplikáciu',
    'tracker' => 'Sledovanie IP a vykresľovanie',
    'authentification' => 'Autentifikácia a autorizácia',
    'news' => 'CRUD pre aktuality',
    'rss' => 'Generovanie RSS',
    'epub' => 'Generovanie ePUB',
    'rest' => 'Rest',
    'csv' => 'Export do CSV',
    'octave' => 'Octave',
    'charts' => 'Grafy',
    'newsletter' => 'Newsletter + verifikácia e-mailu',
    'soap' => 'SOAP',
    'pdf' => 'Generovanie PDF',
    'tech' => 'Technická dokumentácia',
];
