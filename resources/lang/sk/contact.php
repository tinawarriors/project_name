<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Kontakt',
    'title' => 'Informácie o rozdelení úloh',
    'task' => 'Úlohy',
    'togglefilter' => 'Filtrovať',
    'filterplaceholder' => 'Zadaj úlohu/osobu/status',
    'taskcolumns' => 'Úloha',
    'assignee' => 'Zodpovedný',    
    'status' => 'Stav',
];
