<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'SOAP prehľad',
    'functions' => 'Funkcie servicu',
    'examples' => 'Príklady použitia',
    'withwsdl' => 'S wsdl',
    'withoutwsdl' => 'Bez použitia wsdl',
    'pdf' => 'Generovať PDF',
    'epub' => 'Generovať ePUB',
    'uses_library' => 'SOAP service využíva knižnicu',
    'parameter_key' => 'API kľúč',
    'parameter_function' => 'funkcia ktorej hodnoty sa majú vypočítať',
    'parameter_n' => 'počet bodov funkcie ($n >= 2)',
    'parameter_min' => 'počiatočné x',
    'parameter_max' => 'konečné x',
    'return_value_function' => 'návratova hodnota: asociatívne pole dĺžky $n kde kľúč prvkov poľa je x a hodnota prvku je f(x)',
    'return_value_derivation' => 'návratova hodnota: asociatívne pole dĺžky $n kde kľúč prvkov poľa je x a hodnota prvku je (f(x))\''


];
