<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Rest prehľad',
    'pdf' => 'Generovať PDF',
    'epub' => 'Generovať ePUB',
    'parameter' => 'Parameter',
    'description' => 'Popis',
    'value' => 'Hodnota',
    'resource' => 'Funkcia alebo derivácia',
    'key' => 'API kľúč',
    'function' => 'Funkcia',
    'min' => 'Minimum',
    'max' => 'Maximum',
    'n' => 'Počet bodov',
    'request' => 'Žiadosť',
    'response' => 'Odpoveď',
    'errors' => 'Chybové stavy',
    'error' => 'Chyba',
    'example' => 'Príklad',

];
