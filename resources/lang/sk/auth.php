<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name'  =>  'Meno',
    'confirmpassword' => 'Zopakuj heslo',
    'register' => 'Zaregistrovať',
    'email'  => 'Emailová adresa',
    'password' => 'Heslo',
    'remember' => 'Zapamätať si ma',
    'login'    => 'Prihlásenie',
    'forgotpassword' => 'Zabudol si heslo ?',
    'failed' => 'Neznáma požiadavka',
    'throttle' => 'Príliš veľa pokusov o prihlásenie. Skúste o :seconds sekúnd.',

];

