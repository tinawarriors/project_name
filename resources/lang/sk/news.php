<?php

return [
    'new' => 'Nový článok',
    'show' => 'Zobraz článok',
    'header' => 'Aktuality',
    'title' => 'Stále aktuálne',
    'newarticle' => 'Napíš nový článok',
    'publish' => 'Publikuj',
    'edit' => 'Uprav',
    'back' => 'Späť',
    'pdf' => 'Generovať PDF',
    'epub' => 'Generovať ePUB',
    'success' => 'Úspešne pridaný článok',
    'successupdate' => 'Úspešne zmenený článok',      
    'delete' => 'Úspešne zmazaný článok',
    'deleteitem' => 'Vymazať aktualitu',
    'newsletter' => 'Odoberať'
];   
