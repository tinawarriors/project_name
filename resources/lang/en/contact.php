<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Contact overview',
    'title' => 'Here will be information about task list',
    'task' => 'Tasks',
    'togglefilter' => 'Filter',
    'filterplaceholder' => 'Write task/person/status',
    'taskcolumns' => 'Task',
    'assignee' => 'Assignee',
    'status' => 'Status',
];
