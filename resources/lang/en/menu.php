<?php

return [
    'application' => 'Application',
    'rest'        => 'Rest',
    'soap'        => 'Soap',
    'news'        => 'News',
    'tech'        => 'Tech. report',
    'contact'     => 'Contact',
    'home'        => 'Home',
    'login'       => 'Login',
    'register'    => 'Register',
	'rss'         => 'RSS'
];   
