<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Soap overview',
    'functions' => 'Service functions',
    'examples'=> 'Examples',
    'withwsdl'=> 'With wsdl',
    'withoutwsdl'=> 'Withou wsdl',
    'pdf' => 'Generate PDF',
    'epub' => 'Generate ePUB',
    'uses_library' => 'SOAP service uses library',
    'parameter_key' => 'API key',
    'parameter_function' => 'function to compute',
    'parameter_n' => 'number of points ($n >= 2)',
    'parameter_min' => 'start x',
    'parameter_max' => 'end x',
    'return_value_function' => 'return value: associative array of length $n, where key of array element is value x and its value is f(x)',
    'return_value_derivation' => 'return value: associative array of length $n, where key of array element is value x and its value is f(x)\'',
];
