<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'done' => 'Done',
    'inprocess' => 'In process',
    'instalation' => 'Enviroment instalation',
    'gitinstalation' => 'Git instalation',
    'project' => 'Projekt establishment and configuration',
    'localization' => 'Localization',
    'menu' => 'Dynamic menu',
    'layout' => 'Application layout',
    'tracker' => 'IP tracking and visualization',
    'authentification' => 'Authentification and autorization',
    'news' => 'CRUD for news',
    'rss' => 'Generating RSS',
    'epub' => 'Generating ePUB',
    'rest' => 'Rest',
    'csv' => 'Export to CSV',
    'octave' => 'Octave',
    'charts' => 'Charts',
    'newsletter' => 'Newsletter + e-mail verification',
    'soap' => 'SOAP',
    'pdf' => 'Generating PDF',
    'tech' => 'Technical documentation',   
];
