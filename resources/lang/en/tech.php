<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Tech overview',
    'title' => 'Here will be information about technical stuff',
    'octave' => 'Calculating functions',
    'charts' => 'Charts',
    'ePub' => 'Generating ePub',
    'pdf' => 'Generating PDF',
    'soap' => 'Soap',
    'mail' => 'Newsletter',
    'title' => 'The project is based on Laravel 5 framework',
];
