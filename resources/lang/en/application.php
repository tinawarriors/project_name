<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Application',
    'function' => 'Function',
    'derivation' => 'Derivation',
    'min' => 'Min',
    'max' => 'Max',
    'n' => 'Points',
    'submit' => 'Plot',
    'export' => 'Export',
    'working' => 'Working',
    'done' => 'Done',
    'error' => 'Error',
    'notsupported' => 'Not Supported',
    'generateFirstly' => 'Generate Chart Firstly',

];
