<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => 'Rest overview',
    'pdf' => 'Generate PDF',
    'epub' => 'Generate ePUB',
    'parameter' => 'Parameter',
    'description' => 'Description',
    'value' => 'Value',
    'resource' => 'Function or derivation',
    'key' => 'API key',
    'function' => 'Funkction',
    'min' => 'Min',
    'max' => 'Max',
    'n' => 'Points',
    'request' => 'Request',
    'response' => 'Response',
    'errors' => 'Errors',
    'error' => 'Error',
    'example' => 'Example',
];
