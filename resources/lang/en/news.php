<?php

return [
    'new' => 'New article',
    'show' => 'Show article',
    'header' => 'News',
    'title' => 'Always actual',
    'newarticle' => 'Write new article', 
    'publish' => 'Publish',
    'edit' => 'Edit',
    'back' => 'Back',
    'pdf' => 'Generate PDF',
    'epub' => 'Generate ePUB',
    'success' => 'Successfully created news!',
    'successupdate' => 'Successfully updated',
    'delete' => 'Successfully deleted', 
    'deleteitem' => 'Delete this article',
    'newsletter' => 'Subscribe',
];   
