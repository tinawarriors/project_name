<?php
	require "octave/Octave_lib.php";
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	
	$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
	$opt = array_shift($request);
	$funkcia = array_shift($request);
			
	$min = $_GET["min"];	
	$max = $_GET["max"];
	$n = $_GET["n"];
	$key = $_GET["key"];

	$checkNums = is_numeric($min) && is_numeric($max) && is_numeric($n);
	$checkKey = $key=='warriors';
	
	if ($checkNums) {
		$n = floor($n);
		if ($n < 2)
			$checkNums = false;
	}


	$krok = ($max-$min)/($n-1);
	$i=0;

	$o=new Octave(false);
	$o->init();
	 
	$o->registerPartialHandler("process_partial");
	
	$o->run("x = linspace($min, $max,$n);");

	if ($checkKey) {
		if ($checkNums) {
			if ($opt=="function") {
				process($o, $funkcia);
			}
			else if ($opt=="derivation") {
				$query = "diff(x.*" .$funkcia .")./ diff(x)";
				process($o, $query);
			}
			else {
				echo "data:" .json_encode(array("status"=>"error", "description"=>"Resource error.")) ."\n\n";
			}
		
		}
		else {
			echo "data:" .json_encode(array("status"=>"error", "description"=>"Parameters error.")) ."\n\n";
		}
	}
	else {
		echo "data:" .json_encode(array("status"=>"error", "description"=>"API key error.")) ."\n\n";
	}

	function process($o, $query) {
		global $i;
		$o->query($query);
		if ($i==0) {
			echo "data:" .json_encode(array("status"=>"error", "description"=>"Function error.")) ."\n\n";
		}
		else {
			echo "data:" .json_encode(array("status"=>"success")) ."\n\n";
		}
	}
 
	function process_partial($payload,$partial)
	{
		global $krok, $min, $i;
		$parts = explode(" ", $payload);
		foreach ($parts as $y) {
			if (empty($y)) {
				continue;
			}
			$i++;
			usleep(50000);
			if (round($min, 2)==0)
				$min += $krok;

			echo "data:" .json_encode(array("status"=>"partial", "x"=>floatval($min) ,"y"=>floatval($y))) ."\n\n";
			$min += $krok; 
			ob_flush();
	  		flush();
		}
	}
?>
